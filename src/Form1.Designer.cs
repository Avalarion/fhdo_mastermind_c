﻿namespace Mastermind
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label_funde = new System.Windows.Forms.Label();
            this.label_correct = new System.Windows.Forms.Label();
            this.label_position = new System.Windows.Forms.Label();
            this.button_speichern = new System.Windows.Forms.Button();
            this.button_versuchen = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox1.Location = new System.Drawing.Point(43, 39);
            this.textBox1.MaxLength = 1;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(30, 20);
            this.textBox1.TabIndex = 0;
            // 
            // textBox2
            // 
            this.textBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox2.Location = new System.Drawing.Point(79, 39);
            this.textBox2.MaxLength = 1;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(30, 20);
            this.textBox2.TabIndex = 1;
            // 
            // textBox3
            // 
            this.textBox3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox3.Location = new System.Drawing.Point(115, 39);
            this.textBox3.MaxLength = 1;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(30, 20);
            this.textBox3.TabIndex = 2;
            // 
            // textBox4
            // 
            this.textBox4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox4.Location = new System.Drawing.Point(151, 39);
            this.textBox4.MaxLength = 1;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(30, 20);
            this.textBox4.TabIndex = 3;
            // 
            // textBox5
            // 
            this.textBox5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox5.Location = new System.Drawing.Point(187, 39);
            this.textBox5.MaxLength = 1;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(30, 20);
            this.textBox5.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Bitte raten Sie:";
            // 
            // label_funde
            // 
            this.label_funde.AutoSize = true;
            this.label_funde.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_funde.Location = new System.Drawing.Point(12, 78);
            this.label_funde.Name = "label_funde";
            this.label_funde.Size = new System.Drawing.Size(72, 13);
            this.label_funde.TabIndex = 6;
            this.label_funde.Text = "Ihre Funde:";
            this.label_funde.Visible = false;
            // 
            // label_correct
            // 
            this.label_correct.AutoSize = true;
            this.label_correct.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_correct.Location = new System.Drawing.Point(40, 100);
            this.label_correct.Name = "label_correct";
            this.label_correct.Size = new System.Drawing.Size(81, 13);
            this.label_correct.TabIndex = 7;
            this.label_correct.Text = "X Zahlen richtig";
            this.label_correct.Visible = false;
            // 
            // label_position
            // 
            this.label_position.AutoSize = true;
            this.label_position.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_position.Location = new System.Drawing.Point(40, 123);
            this.label_position.Name = "label_position";
            this.label_position.Size = new System.Drawing.Size(145, 13);
            this.label_position.TabIndex = 8;
            this.label_position.Text = "X Zahlen an richtiger Position";
            this.label_position.Visible = false;
            // 
            // button_speichern
            // 
            this.button_speichern.Location = new System.Drawing.Point(311, 23);
            this.button_speichern.Name = "button_speichern";
            this.button_speichern.Size = new System.Drawing.Size(75, 23);
            this.button_speichern.TabIndex = 9;
            this.button_speichern.Text = "Speichern";
            this.button_speichern.UseVisualStyleBackColor = true;
            this.button_speichern.Click += new System.EventHandler(this.buttonSpeichern_Click);
            // 
            // button_versuchen
            // 
            this.button_versuchen.Enabled = false;
            this.button_versuchen.Location = new System.Drawing.Point(311, 67);
            this.button_versuchen.Name = "button_versuchen";
            this.button_versuchen.Size = new System.Drawing.Size(75, 23);
            this.button_versuchen.TabIndex = 10;
            this.button_versuchen.Text = "Versuchen";
            this.button_versuchen.UseVisualStyleBackColor = true;
            this.button_versuchen.Click += new System.EventHandler(this.buttonTesten_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 425);
            this.Controls.Add(this.button_versuchen);
            this.Controls.Add(this.button_speichern);
            this.Controls.Add(this.label_position);
            this.Controls.Add(this.label_correct);
            this.Controls.Add(this.label_funde);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_funde;
        private System.Windows.Forms.Label label_correct;
        private System.Windows.Forms.Label label_position;
        private System.Windows.Forms.Button button_speichern;
        private System.Windows.Forms.Button button_versuchen;
    }
}

