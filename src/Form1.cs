﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mastermind
{
    public partial class Form1 : Form
    {
        protected char[] test;
        protected int length = 5;
        protected char placeholder = 'a';

        public Form1()
        {

            InitializeComponent();
            this.test = new char[this.length];
        }

        private void buttonSpeichern_Click(object sender, EventArgs e)
        {
            try
            {
                test[0] = this.textBox1.Text[0];
                test[1] = this.textBox2.Text[0];
                test[2] = this.textBox3.Text[0];
                test[3] = this.textBox4.Text[0];
                test[4] = this.textBox5.Text[0];
                this.textBox1.Text = "";
                this.textBox2.Text = "";
                this.textBox3.Text = "";
                this.textBox4.Text = "";
                this.textBox5.Text = "";
                button_versuchen.Enabled = true;
                button_speichern.Enabled = false;
            }
            catch (IndexOutOfRangeException exception)
            {
                MessageBox.Show("Bitte alle Felder ausfüllen.", "Fehler");
            }
        }

        private void buttonTesten_Click(object sender, EventArgs e)
        {
            try
            {
                bool richtig = true;
                int position = 0;
                int values = 0;
                for (int i = 0; i < test.Length; i++)
                    Console.WriteLine(i + ": " + test[i]);

                /**
                 * Check Positions
                 */
                if (test[0] == this.textBox1.Text[0]) position++;
                if (test[1] == this.textBox2.Text[0]) position++;
                if (test[2] == this.textBox3.Text[0]) position++;
                if (test[3] == this.textBox4.Text[0]) position++;
                if (test[4] == this.textBox5.Text[0]) position++;
                if (position != this.length) richtig = false;

                /**
                 * Check Values
                 */
                char[] helper = new char[this.length];
                Array.Copy(this.test, helper, this.length);
                helper = this.checkValues(this.textBox1.Text[0], helper);
                helper = this.checkValues(this.textBox2.Text[0], helper);
                helper = this.checkValues(this.textBox3.Text[0], helper);
                helper = this.checkValues(this.textBox4.Text[0], helper);
                helper = this.checkValues(this.textBox5.Text[0], helper);
                for (int i = 0; i < helper.Length; i++)
                {
                    if (helper[i] == this.placeholder) values++;
                }
                if (values != this.length) richtig = false;
                if (richtig)
                {
                    button_versuchen.Enabled = false;
                    button_speichern.Enabled = true;
                    this.textBox1.Text = "";
                    this.textBox2.Text = "";
                    this.textBox3.Text = "";
                    this.textBox4.Text = "";
                    this.textBox5.Text = "";
                    label_correct.Visible = false;
                    label_funde.Visible = false;
                    label_position.Visible = false;
                    MessageBox.Show("Sie haben es Geschafft!", "Gratulation");
                }
                else
                {
                    label_funde.Visible = true;
                    label_correct.Visible = true;
                    label_position.Visible = true;
                    label_correct.Text = values + " Zahlen richtig";
                    label_position.Text = position + " Zahlen an richtiger Position";
                }
                
            }
            catch (IndexOutOfRangeException exception)
            {
                MessageBox.Show("Bitte alle Felder ausfüllen.", "Fehler");
            }
        }

        protected char[] checkValues(char test, char[] helper)
        {
            for (int i = 0; i < helper.Length; i++)
                {
                    if (helper[i] == test)
                    {
                        helper[i] = this.placeholder;
                        return helper;
                    }
                }
            return helper;
        }
    }
}
